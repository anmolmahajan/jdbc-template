package com.notsujata.notdemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class MyMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement st = null;
		ResultSet res = null;
		PreparedStatement ps = null;
		
		final String USERNAME = "hr";
		final String PASSWORD = "hr";
		
		
		Scanner s = new Scanner(System.in);
		// TODO Auto-generated method stub
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", USERNAME, PASSWORD);
			while(true) {
				
				System.out.println("1. Show Employees");
				System.out.println("2. Insert Employee");
				System.out.println("3. Search Employee");
				int choice  = s.nextInt();
				switch(choice) {
					case 1:	st = conn.createStatement();
							res = st.executeQuery("select * from employees where employee_id < 110");
							while(res.next()) {
								System.out.println("ID: " + res.getInt(1));
								System.out.println("Name: " + res.getString(2) + " " + res.getString(3));
							}
							break;
					case 2: String insertTableSQL = "INSERT INTO EMPLOYEES"
							+ "(EMPLOYEE_ID, FIRST_NAME, LAST_NAME, EMAIL, HIRE_DATE, JOB_ID) VALUES"
							+ "(?,?,?,?,?,?)";
							ps = conn.prepareStatement(insertTableSQL);
							ps.setInt(1, 700);
							ps.setString(2, "Harsh");
							ps.setString(3, "Gupta");
							ps.setString(4, "hg@gmail.com");
							ps.setString(5, "5-JUL-2017");
							ps.setString(6, "ST_MAN");
							
							System.out.println(ps.executeUpdate());
							
					case 3: st = conn.createStatement();
							int inp_id = s.nextInt();
							res = st.executeQuery("select * from employees where employee_id=" + inp_id);
							while(res.next()) {
								System.out.println("ID: " + res.getInt(1));
								System.out.println("Name: " + res.getString(2) + " " + res.getString(3));
							}break;
					
					default: System.exit(-1);
				}
			}
			
			
				
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
