package com.notsujata.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class MyJDBCDemo {
	private static String USERNAME = "hr";
	private static String PASSWORD = "hr";
	public static void main(String[] args) {
		Connection conn = null;
		// TODO Auto-generated method stub
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", USERNAME, PASSWORD);
			
			Statement st = conn.createStatement(); //Prepared statements for dynamic queries
			ResultSet res = st.executeQuery("select * from employees where employee_id < 110");
			
			//CallableStatement cs = conn.prepareCall("{call prd1}"); //prd1 is a procedure
			
			
			while(res.next()) {
				int id = res.getInt("employee_id");
				String name = res.getString("first_name");
				System.out.println("Id: " + id + ", Name: " + name);
			}
				
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
